package ru.t1.karimov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    @Column(nullable = false)
    private Date date = new Date();

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

    public Session(@NotNull final User user, @Nullable final Role role) {
        super(user);
        this.role = role;
    }

}
