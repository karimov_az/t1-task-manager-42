package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.enumerated.TaskSort;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDto add(@NotNull TaskDto model) throws Exception;

    @NotNull
    TaskDto changeTaskStatusById (
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    TaskDto changeTaskStatusByIndex (
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<TaskDto> findAll() throws Exception;

    @NotNull
    List<TaskDto> findAll(@Nullable String userId) throws Exception;

    @NotNull
    List<TaskDto> findAll(@Nullable String userId, @Nullable TaskSort sort) throws Exception;

    @Nullable
    TaskDto findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    TaskDto findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @NotNull
    List<TaskDto> findAllByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void removeAll() throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeOne(@Nullable String userId, @Nullable TaskDto model) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    void set(@NotNull Collection<TaskDto> tasks) throws Exception;

    @NotNull
    TaskDto updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    TaskDto updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
