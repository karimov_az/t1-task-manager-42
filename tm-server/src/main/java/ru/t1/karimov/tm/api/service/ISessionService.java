package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.SessionDto;

import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDto add(@Nullable SessionDto model) throws  Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<SessionDto> findAll(@Nullable String userId) throws Exception;

    @Nullable
    SessionDto findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    SessionDto findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    SessionDto removeOne(@Nullable String userId, @Nullable SessionDto model) throws Exception;

    @Nullable
    SessionDto removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    SessionDto removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @NotNull
    SessionDto update(@Nullable String userId, @Nullable SessionDto session) throws Exception;

}
