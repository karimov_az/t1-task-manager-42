package ru.t1.karimov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.SessionDto;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session " +
            "(id, created, user_id, date, role) " +
            "VALUES(#{id}, #{created}, #{userId}, #{date}, #{role})"
    )
    void add(@NotNull SessionDto model);

    @Select("SELECT * FROM tm_session")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
    })
    @NotNull
    List<SessionDto> findAll();

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<SessionDto> findAllByUserId(@NotNull String userId);

    @Select("SELECT * FROM tm_session " +
            "WHERE user_id = #{userId} " +
            "ORDER BY #{comparator}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<SessionDto> findAllByUserIdSorted(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("comparator") String comparator
    );

    @Select("SELECT * FROM tm_session " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    SessionDto findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT count(*) > 0 FROM tm_session " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1"
    )
    boolean existById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT count(*) FROM tm_session")
    int getSize();

    @Select("SELECT count(*) FROM tm_session WHERE user_id = #{userId}")
    int getSizeByUserId(@NotNull String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void removeAll(@NotNull String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void removeOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Update("UPDATE tm_session SET" +
            " user_id = #{userId}," +
            " date = #{date}," +
            " role = #{role} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull SessionDto model);

}
