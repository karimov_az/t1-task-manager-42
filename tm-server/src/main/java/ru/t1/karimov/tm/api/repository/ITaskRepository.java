package ru.t1.karimov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task " +
            "(id, created, name, description, status, user_id, project_id) " +
            "VALUES(#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})"
    )
    void add(@NotNull TaskDto model);

    @Select("SELECT count(*) > 0 FROM tm_task " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1"
    )
    boolean existsById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @NotNull
    List<TaskDto> findAll();

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @NotNull
    List<TaskDto> findAllByUserId(@NotNull String userId);

    @Select("SELECT * FROM tm_task " +
            "WHERE user_id = #{userId} " +
            "ORDER BY ${sort}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<TaskDto> findAllByUserIdSorted(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("sort") String sort
    );

    @Select("SELECT * FROM tm_task " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable
    TaskDto findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull
    List<TaskDto> findAllByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

    @Select("SELECT count(*) FROM tm_task")
    int getSize();

    @Select("SELECT count(*) FROM tm_task WHERE user_id = #{userId}")
    int getSizeByUserId(@NotNull String userId);

    @Delete("TRUNCATE TABLE tm_task")
    void removeAll();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void removeAllByUserId(@NotNull String userId);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Update("UPDATE tm_task SET" +
            " name = #{name}," +
            " description = #{description}," +
            " status = #{status}," +
            " user_id = #{userId}," +
            " project_id = #{projectId} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull TaskDto model);

}
