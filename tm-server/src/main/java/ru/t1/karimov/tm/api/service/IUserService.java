package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Role;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    UserDto add(@NotNull UserDto model) throws Exception;

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    List<UserDto> findAll() throws Exception;

    @Nullable
    UserDto findByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDto findByEmail(@Nullable String email) throws Exception;

    @Nullable
    UserDto findOneById(@Nullable String id) throws Exception;

    @Nullable
    UserDto findOneByIndex(@Nullable Integer index) throws Exception;

    int getSize() throws Exception;

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    void lockUserByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDto removeOne(@Nullable UserDto model) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    UserDto removeOneByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDto removeOneByEmail(@Nullable String email) throws Exception;

    void set(@NotNull Collection<UserDto> users) throws Exception;

    @NotNull
    UserDto setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    UserDto updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

}
