package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.enumerated.ProjectSort;
import ru.t1.karimov.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDto add(@NotNull ProjectDto model) throws Exception;

    @NotNull
    ProjectDto changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    ProjectDto changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    ProjectDto create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    ProjectDto create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull List<ProjectDto> findAll() throws Exception;

    @NotNull
    List<ProjectDto> findAll(@Nullable String userId) throws Exception;

    @NotNull
    List<ProjectDto> findAll(@Nullable String userId, @Nullable ProjectSort sort) throws Exception;

    @Nullable
    ProjectDto findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    ProjectDto findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void removeOne(@Nullable String userId, @Nullable ProjectDto model) throws Exception;

    void removeAll() throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    void set(@NotNull Collection<ProjectDto> projects) throws Exception;

    @NotNull
    ProjectDto updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    ProjectDto updateProjectByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
