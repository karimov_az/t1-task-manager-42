package ru.t1.karimov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.ProjectDto;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project " +
            "(id, created, name, description, status, user_id) " +
            "VALUES(#{id}, #{created}, #{name}, #{description}, #{status}, #{userId})"
    )
    void add(@NotNull ProjectDto model);

    @Select("SELECT count(*) > 0 FROM tm_project WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_project")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<ProjectDto> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull
    List<ProjectDto> findAllByUserId(@NotNull String userId);

    @Select("SELECT * FROM tm_project " +
            "WHERE user_id = #{userId} " +
            "ORDER BY ${sort}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @NotNull
    List<ProjectDto> findAllByUserIdSorted(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("sort") String sort
    );

    @Select("SELECT * FROM tm_project " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable
    ProjectDto findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT count(*) FROM tm_project WHERE user_id = #{userId}")
    int getSizeByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT count(*) FROM tm_project")
    int getSize();

    @Delete("TRUNCATE TABLE tm_project")
    void removeAll();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void removeAllByUserId(@NotNull String userId);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Update("UPDATE tm_project SET" +
            " name = #{name}," +
            " description = #{description}," +
            " status = #{status}," +
            " user_id = #{userId} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull ProjectDto model);

}
