package ru.t1.karimov.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.enumerated.TaskSort;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.service.ConnectionService;
import ru.t1.karimov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final int HALF_NUMBER_OF_ENTRIES = NUMBER_OF_ENTRIES / 2;

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static String PROJECT1_ID = "";

    @NotNull
    private static String PROJECT2_ID = "";

    @NotNull
    private static List<TaskDto> taskList;

    @NotNull
    private static List<ProjectDto> projectList;

    @NotNull
    private static ITaskRepository taskRepository;

    @NotNull
    private static IProjectRepository projectRepository;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final SqlSession connection = connectionService.getSqlSession();

    @NotNull
    private static final IUserRepository userRepository = connection.getMapper(IUserRepository.class);

    @NotNull
    private static String getSortType(@NotNull final TaskSort sort) {
        if (sort == TaskSort.BY_CREATED) return "created";
        if (sort == TaskSort.BY_STATUS) return "status";
        else return "name";
    }

    @BeforeClass
    public static void createUsers() throws Exception {
        @NotNull final UserDto user1 = new UserDto();
        user1.setLogin("test1");
        user1.setPasswordHash("test1");
        userRepository.add(user1);
        connection.commit();
        USER1_ID = user1.getId();

        @NotNull final UserDto user2 = new UserDto();
        user2.setLogin("test2");
        user2.setPasswordHash("test2");
        userRepository.add(user2);
        connection.commit();
        USER2_ID = user2.getId();
    }

    @Before
    public void initRepository() throws Exception {
        projectList = new ArrayList<>();
        projectRepository = connection.getMapper(IProjectRepository.class);
        taskList = new ArrayList<>();
        taskRepository = connection.getMapper(ITaskRepository.class);
        for (int i = 0; i < 2; i++) {
            @NotNull final ProjectDto project = new ProjectDto();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i == 0) {
                project.setUserId(USER1_ID);
                PROJECT1_ID = project.getId();
            }
            else {
                project.setUserId(USER2_ID);
                PROJECT2_ID = project.getId();
            }
            projectList.add(project);
            projectRepository.add(project);
            connection.commit();

            for (int j = 0; j < HALF_NUMBER_OF_ENTRIES; j++) {
                @NotNull final TaskDto task = new TaskDto();
                task.setName("Task " + j);
                task.setDescription("Description " + j);
                if (j == 1 || j == 3 ) task.setStatus(Status.IN_PROGRESS);
                if (i == 0) {
                    task.setUserId(USER1_ID);
                    task.setProjectId(PROJECT1_ID);
                }
                else {
                    task.setUserId(USER2_ID);
                    task.setProjectId(PROJECT2_ID);
                }
                taskRepository.add(task);
                taskList.add(task);
                connection.commit();
            }
        }
    }

    @AfterClass
    public static void clearUsers() throws Exception {
        userRepository.removeOneById(USER1_ID);
        connection.commit();
        userRepository.removeOneById(USER2_ID);
        connection.commit();
        connection.close();
    }

    @After
    public void initClear() throws Exception {
        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            assertNotNull(userId);
            taskRepository.removeOneById(userId, task.getId());
            connection.commit();
        }
        for (@NotNull final ProjectDto project : projectList) {
            @Nullable final String userId = project.getUserId();
            assertNotNull(userId);
            projectRepository.removeOneById(userId, project.getId());
            connection.commit();
        }
        taskList.clear();
        projectList.clear();
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final TaskDto task = new TaskDto();
        @NotNull final String name = "Test Project Name";
        @NotNull final String description = "Test Project Description";
        @NotNull final String id = task.getId();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(USER1_ID);
        taskRepository.add(task);
        connection.commit();
        @Nullable final TaskDto actualTask = taskRepository.findOneById(USER1_ID, id);
        assertNotNull(actualTask);
        assertEquals(USER1_ID, actualTask.getUserId());
        assertEquals(name, actualTask.getName());
        assertEquals(description, actualTask.getDescription());

        @Nullable final String userId = actualTask.getUserId();
        assertNotNull(userId);
        taskRepository.removeOneById(userId, actualTask.getId());
        connection.commit();
    }

    @Test
    public void testClearForUserPositive() throws Exception {
        @NotNull final List<TaskDto> emptyList = new ArrayList<>();
        taskRepository.removeAllByUserId(USER1_ID);
        connection.commit();
        assertEquals(emptyList, taskRepository.findAllByUserId(USER1_ID));
        assertNotEquals(emptyList, taskRepository.findAllByUserId(USER2_ID));
    }

    @Test
    public void testClearForUserNegative() throws Exception {
        taskRepository.removeAllByUserId("Other_id");
        connection.commit();
        final int numberAllTasks = taskRepository.getSizeByUserId(USER1_ID) + taskRepository.getSizeByUserId(USER2_ID);
        assertEquals(NUMBER_OF_ENTRIES, numberAllTasks);
    }

    @Test
    public void testFindAllByUser() throws Exception {
        @NotNull final List<TaskDto> expectedTasks = taskList.stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<TaskDto> actualTasks = taskRepository.findAllByUserId(USER1_ID);
        assertEquals(expectedTasks.size(), actualTasks.size());
    }

    @Test
    public void testFindAllSortByUser() throws Exception {
        for (@NotNull final TaskSort sort : TaskSort.values()) {
            @NotNull final List<TaskDto> actualTaskList = taskRepository.findAllByUserIdSorted(USER1_ID, getSortType(sort));
            @NotNull final List<TaskDto> expectedTaskList = taskList.stream()
                    .filter(m -> USER1_ID.equals(m.getUserId()))
                    .sorted(sort.getComparator())
                    .collect(Collectors.toList());
            assertEquals(expectedTaskList.size(), actualTaskList.size());
        }
    }

    @Test
    public void testFindOneByIdForUserPositive() throws Exception {
        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = task.getId();
            @Nullable final TaskDto actualTask = taskRepository.findOneById(userId, id);
            assertNotNull(actualTask);
            assertEquals(id, actualTask.getId());
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() throws Exception {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            assertNull(taskRepository.findOneById(USER1_ID, UUID.randomUUID().toString()));
        }
    }

    @Test
    public void testFindAllByProjectIdForUserPositive() throws Exception {
        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        for (@NotNull final String userId : userList) {
            @NotNull final String projectId = userId.equals(USER1_ID) ? PROJECT1_ID : PROJECT2_ID;
            @NotNull final List<TaskDto> expectedList = taskList.stream()
                    .filter(m -> m.getProjectId() != null)
                    .filter(m -> userId.equals(m.getUserId()))
                    .filter(m -> projectId.equals(m.getProjectId()))
                    .collect(Collectors.toList());
            @NotNull final List<TaskDto> actualList = taskRepository.findAllByProjectId(userId, projectId);
            assertEquals(expectedList.size(), actualList.size());
        }
    }

    @Test
    public void testFindAllByProjectIdForUserNegative() throws Exception {
        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        for (@NotNull final String userId : userList) {
            @NotNull final String projectId = userId.equals(USER1_ID) ? PROJECT2_ID : PROJECT1_ID;
            @NotNull final List<TaskDto> actualList = taskRepository.findAllByProjectId(userId, projectId);
            assertEquals(0, actualList.size());
        }
    }

    @Test
    public void testExistByIdForUserPositive() throws Exception {
        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = task.getId();
            assertTrue(taskRepository.existsById(userId, id));
        }
    }

    @Test
    public void testExistByIdForUserNegative() throws Exception {
        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            if (userId == null) throw new UserIdEmptyException();
            @NotNull final String id = UUID.randomUUID().toString();
            assertFalse(projectRepository.existsById(userId, id));
        }
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        for (@NotNull final String userId : userList) {
            final int taskRepositorySize = taskRepository.getSizeByUserId(userId);
            @Nullable final List<TaskDto> actualList = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(actualList.size(), taskRepositorySize);
        }
    }

    @Test
    public void testRemoveByIdForUserPositive() throws Exception {
        for (@NotNull final TaskDto task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            assertNotNull(userId);
            taskRepository.removeOneById(userId, id);
            connection.commit();
            assertNull(taskRepository.findOneById(userId, id));
        }
    }

    @Test
    public void testRemoveByIdForUserNegative() throws Exception {
        final int expectedSize = taskRepository.getSize();
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        @NotNull final String otherId = UUID.randomUUID().toString();
        taskRepository.removeOneById(USER1_ID, otherId);
        connection.commit();
        taskRepository.removeOneById(USER2_ID, otherId);
        connection.commit();
        taskRepository.removeOneById(otherUserId,otherId);
        connection.commit();
        final int actualSize = taskRepository.getSize();
        assertEquals(expectedSize, actualSize);
    }

}
